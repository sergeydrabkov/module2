﻿using System;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();

            Console.WriteLine("--Task1--");
            Console.WriteLine(p.GetTotalTax(1, 100, 100));

            Console.WriteLine("--Task2--");
            Console.WriteLine(p.GetCongratulation(20));
            Console.WriteLine(p.GetCongratulation(15));
            Console.WriteLine(p.GetCongratulation(21));
            Console.WriteLine(p.GetCongratulation(10));

            Console.WriteLine("--Task3--"); 
            Console.WriteLine(p.GetMultipliedNumbers("123,456", "1000"));
            Console.WriteLine(p.GetMultipliedNumbers("123.456", "1000"));

            Console.WriteLine("--Task4--");
            Dimensions d = new Dimensions();
            d.FirstSide = 38;
            d.SecondSide = 36;
            d.ThirdSide = 17;
            d.Height = 0;
            d.Diameter = 50;
            d.Radius = 25;

            Console.WriteLine(p.GetFigureValues(FigureEnum.Triangle, ParameterEnum.Square, d));

            d.FirstSide = 10;
            d.SecondSide = 20;
            d.ThirdSide = 25;
            d.Height = 0;
            d.Diameter = 50;
            d.Radius = 25;

            Console.WriteLine(p.GetFigureValues(FigureEnum.Triangle, ParameterEnum.Square, d));
            
            d.FirstSide = 0;
            d.SecondSide = 0;
            d.ThirdSide = 0;
            d.Height = 0;
            d.Diameter = 10.5d;
            d.Radius = 5.25d;

            Console.WriteLine(p.GetFigureValues(FigureEnum.Circle, ParameterEnum.Square, d));

            d.FirstSide = 10;
            d.SecondSide = 20;
            d.ThirdSide = 0;
            d.Height = 0;
            d.Diameter = 0;
            d.Radius = 0;

            Console.WriteLine(p.GetFigureValues(FigureEnum.Rectangle, ParameterEnum.Perimeter, d));

            d.FirstSide = 38;
            d.SecondSide = 0;
            d.ThirdSide = 0;
            d.Height = 12;
            d.Diameter = 50;
            d.Radius = 25;

            Console.WriteLine(p.GetFigureValues(FigureEnum.Triangle, ParameterEnum.Square, d));
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            return companiesNumber * companyRevenue * tax / 100;
        }

        public string GetCongratulation(int input)
        {
            string str;
            bool even = input % 2 == 0;

            if (even && input >= 18)
            {
                str = "Поздравляю с совершеннолетием!";
            }
            else if (!even && input < 18 && input > 12)
            {
                str = "Поздравляю с переходом в старшую школу!";
            }
            else
            {
                str = "Поздравляю с " + input + "-летием!";
            }

            return str;
        }

        public double GetMultipliedNumbers(string first, string second)
        {

            if (System.Globalization.CultureInfo.CurrentCulture.ToString() == "ru-RU")
            {
                first = first.Replace(".", ",");
                second = second.Replace(".", ",");
            }
            else
            {
                first = first.Replace(",", ".");
                second = second.Replace(",", ".");
            }
            return double.Parse(first) * double.Parse(second);
        }

        public double GetFigureValues(FigureEnum figureType, ParameterEnum parameterToCompute, Dimensions dimensions)
        {
            double s = 0, p = 0, v = 0;

            switch (figureType)
            {
                case FigureEnum.Triangle:
                    if ((dimensions.FirstSide > 0) && (dimensions.Height > 0))
                    {
                        s = dimensions.FirstSide * dimensions.Height / 2;
                        p = dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide;
                    }
                    else if ((dimensions.FirstSide > 0) && (dimensions.SecondSide > 0) && (dimensions.ThirdSide > 0))
                    {                      
                        p = dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide;
                        s = (int)Math.Sqrt(p / 2 * (p / 2 - dimensions.FirstSide) * (p / 2 - dimensions.SecondSide) * (p / 2 - dimensions.ThirdSide));
                    }
                    break;

                case FigureEnum.Rectangle:

                    if ((dimensions.FirstSide > 0) || (dimensions.SecondSide > 0))
                    { 
                        s = dimensions.FirstSide * dimensions.SecondSide;
                        p = 2 * (dimensions.FirstSide + dimensions.SecondSide);
                    }

                    break;

                case FigureEnum.Circle:

                    if (dimensions.Radius > 0)
                    {
                        s = Math.PI * dimensions.Radius * dimensions.Radius;
                        p = Math.PI * dimensions.Radius * 2;
                    }
                    else if (dimensions.Diameter > 0)
                    {
                        s = Math.PI * dimensions.Diameter * dimensions.Diameter/4;
                        p = Math.PI * dimensions.Diameter;
                    }

                    break;
            }

            switch (parameterToCompute)
            {
                case ParameterEnum.Square:
                    v = s;
                    break;
                case ParameterEnum.Perimeter:
                    v = p;
                    break;
            }

            return Math.Round(v);
        }
    }
}
